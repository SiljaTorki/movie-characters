﻿using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    public interface ICharacterService
    {
        /// <summary>
        /// Get all characters.
        /// </summary>
        /// <returns>Collection of all characters in the database.</returns>
        public Task<IEnumerable<Character>> GetAllCharacterAsync();

        /// <summary>
        /// Get character by ID.
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns></returns>
        public Task<Character> GetSpecificCharacterAsync(int id);

        /// <summary>
        /// Add character to database.
        /// </summary>
        /// <param name="character">Character to add</param>
        /// <returns>The character added into the database if successful.</returns>
        public Task<Character> AddCharacterAsync(Character character);

        /// <summary>
        /// Update character in database.
        /// </summary>
        /// <param name="character">Character with updated attributes</param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);

        /// <summary>
        /// Delete a character from the database.
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int id);

        /// <summary>
        /// Check if character with the given ID exists in the database.
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns>True if character exists.</returns>
        public bool CharacterExists(int id);

    }
}
