﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Data;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.CharacterDTOs;
using MovieCharactersAPI.Models.DTOs.MovieDTOs;
using MovieCharactersAPI.Services;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        // Add automapper thorugh DI
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;
        private readonly ICharacterService _characterService;

        public MoviesController(MovieCharactersDbContext context, IMapper mapper, 
            IMovieService movieService, ICharacterService characterService)
        {
            _context = context;
            _mapper = mapper;
            _movieService = movieService;
            _characterService = characterService;
        }

        /// <summary>
        ///     Get all Movies in the database
        /// </summary>
        /// <returns>Collection of all movies in database.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMovieAsync());
        }

        /// <summary>
        ///     Gets a specific Movie by their ID.
        /// </summary>
        /// <param name="id">Movie ID.</param>
        /// <returns>Movie if it exists</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound($"Movie with id={id} not found");
            }

            var movie = _mapper.Map<MovieReadDTO>(await _movieService.GetSpecificMovieAsync(id));

            return movie;
        }

        /// <summary>
        ///     Updates a Movie.
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <param name="movieDto">MovieEditDTO with updated attributes.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieDto)
        {
            if (id != movieDto.ID)
            {
                return BadRequest($"Movie with id={id} not found");
            }

            //Map to domain
            Movie domainMovie = _mapper.Map<Movie> (movieDto);

            try
            {
                await _movieService.UpdateMovieAsync(domainMovie);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_movieService.MovieExists(id))
                {
                    return NotFound($"Movie with id={id} not found");
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        ///     Adds a new Movie to the database.
        /// </summary>
        /// <param name="movieDTO">MovieCreateDTO</param>
        /// <returns>Movie object if added successfully to the database.</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO movieDTO)
        {
            Movie domainMovie = _mapper.Map<Movie>(movieDTO);

            try
            {
                domainMovie = await _movieService.AddMovieAsync(domainMovie);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return CreatedAtAction("GetMovie", 
                new { id = domainMovie.ID },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        ///     Deletes a Movie from the database.
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound($"Movie with id={id} not found");
            }

            try
            {
                await _movieService.DeleteMovieAsync(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Get all characters in this movie.
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns>All characters in the given movie.</returns>
        [HttpGet("characters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInMovie(int id)
        {
            
            if (!_movieService.MovieExists(id))
            {
                return NotFound($"Movie with id={id} not found");
            }

            var movie = _mapper.Map<MovieReadDTO>(await _movieService.GetSpecificMovieAsync(id));

            List<CharacterReadDTO> movieCharacters = new();

            foreach (int characterID in movie.Characters)
            {
                if (!_characterService.CharacterExists(characterID))
                    return NotFound($"Character with id={characterID} is not found");

                var character = _mapper.Map<CharacterReadDTO>(
                    await _characterService.GetSpecificCharacterAsync(characterID));

                movieCharacters.Add(character);

            }

            return movieCharacters;
        }

        /// <summary>
        /// Updates Characters in Movie.
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <param name="characterIDs">List of the Character IDs of the characters you want this movie to have.</param>
        /// <returns></returns>
        [HttpPut("character/{id}")]
        public async Task<IActionResult> PutCharacterInMovie(int id, List<int> characterIDs)
        {

            if (!_movieService.MovieExists(id))
            {
                return NotFound($"Movie with id={id} not found");
            }

            Movie movieToUpdate = await _movieService.GetSpecificMovieAsync(id);

            List<Character> characters = new();
            foreach (int characterID in characterIDs)
            {
                if (!_characterService.CharacterExists(characterID)) 
                    return NotFound($"Character with id={characterID} is not found");

                Character character = await _characterService.GetSpecificCharacterAsync(characterID);
                characters.Add(character);
            }

            movieToUpdate.Characters = characters;

            try
            {

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();

        }

    }
}
