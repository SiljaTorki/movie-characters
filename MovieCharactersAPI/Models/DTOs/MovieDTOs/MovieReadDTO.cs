﻿using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTOs.MovieDTOs
{
    public class MovieReadDTO
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public ICollection<int> Characters { get; set; }
        public int FranchiseID { get; set; }
    }
}
