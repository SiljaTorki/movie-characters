# Movie Characters API 

An Entity Framework Code First workflow and ASP.NET Core Web API in C#.

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Description](#description)
- [Maintainers](#maintainers)
- [Project status](#project-status)


## Installation

Install following tools:

* Visual Studio 2019 with .NET 5
* SQL Server Managment Studio

## Usage

First clone the git repository

    git clone git@gitlab.com:SiljaTorki/movie-characters.git

Open in Visual Studio.

Connect to local server (change this line inn appsettings.json"):

    "DefaultConnection": "Data Source = <<NAME OF CONNECTION STRING>>; Initial Catalog = MovieCharactersDb; Integrated Security = True;"

Open Package Manager Console in Visual Studio an run to create the database in LocalDb:

    update-database

Run program with button with the *green arrow and IIS Express* or *F5*

## Description

### Models
* DTOs - Contains Create, Read and Edit classes for Movie, Character and Franchise tables
* Data - Contains information abput the database, like setting realtionship between tables and dummydata
* Domain - Contains the Movie, Character and Franchise tables in the database

### Migrations
* Conntains the information about hte database that will be created when running ```update-database```


### Profiles
* CharacterProfile - Relation between CharacterDTOs and Character
* FranchiseProfile - Relation between FranchiseDTOs and Franchise
* MovieProfile - Relation between MovieDTOs and Movie

### Controllers

#### CharacterController
Contains HttpRequests for Character

* GetCharacter() - Get all Characters in the database.
* GetCharacter(int id) - Gets a specific Character by their ID.
* PutCharacter(int id, CharacterEditDTO characterDTO) -  Updates a Character.
* PostCharacter(CharacterCreateDTO characterDTO) - Adds a new Character to the database.
* DeleteCharacter(int id) - Deletes a Character from the database.

#### FranchisesController
Contains HttpRequests for Franchise

* GetFranchises() - Get all Franchises in the database.
* GetFranchise(int id) - Gets a specific Franchise by their ID.
* PutFranchise(int id, FranchiseEditDTO franchiseDTO) - Updates a Franchise.
* PostFranchise(FranchiseCreateDTO franchiseDTO) - Adds a new Franchise to the database.
* DeleteFranchise(int id) - Deletes a Franchise from the database.
* GetAllMoviesInFranchise(int id) - Get all Movies in Franchise.
* GetAllCharactersInFranchise(int id) - Get all Characters in Franchise.
* PutMoviesInFranchise(int id, List<int> movieIDs) - Updates Movies in Franchise.

#### MoviesController
Contains HttpRequests for Movie
* GetMovies() - Get all Movies in the database.
* GetMovie(int id) - Gets a specific Movie by their ID.
* PutMovie(int id, MovieEditDTO movieDto) - Updates a Movie.
* PostMovie(MovieCreateDTO movieDTO) - Adds a new Movie to the database.
* DeleteMovie(int id) - Deletes a Movie from the database.
* GetAllCharactersInMovie(int id) - Get all characters in this movie. 
* PutCharacterInMovie(int id, List<int> characterIDs) - Updates Characters in Movie.

### Services
* CharacterService - Contains helping methods that frees CharacterController
* FranchiseService - Contains helping methods that frees FranchiseController
* MovieService - Contains helping methods that frees MovieController

### Visuals
![ER Diagram of the MovieCharacters database](ERdiagramMovieCharactersDb.png)
ER Diagram of the MovieCharacters database.

## Maintainers

Karianne Tesaker - @kariannet

Silja Stubhag Torkildsen - @SiljaTorki

## Project status

Complete!
