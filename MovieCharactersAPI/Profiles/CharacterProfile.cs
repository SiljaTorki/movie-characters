﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.CharacterDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            // Character <-> CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>()
                // Turning related movies into arrays
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.ID).ToArray()))
                .ReverseMap();

            // CharacterCreateDTO <-> Character
            CreateMap<CharacterCreateDTO, Character>().ReverseMap();

            // CharacterEditDTO <-> Character
            CreateMap<CharacterEditDTO, Character>().ReverseMap(); 
        }
    }
}
