﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Data;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.CharacterDTOs;
using MovieCharactersAPI.Models.DTOs.FranchiseDTOs;
using MovieCharactersAPI.Models.DTOs.MovieDTOs;
using MovieCharactersAPI.Services;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        // Add automapper thorugh DI
        private readonly IMapper _mapper;

        private readonly IFranchisesServise _franchisesServise;

        private readonly IMovieService _movieService;

        private readonly ICharacterService _characterService;
        public FranchisesController(MovieCharactersDbContext context, IMapper mapper, IFranchisesServise franchisesServise, IMovieService movieService , ICharacterService characterService)
        {
            _context = context;
            _mapper = mapper;
            _franchisesServise = franchisesServise;
            _movieService = movieService;
            _characterService = characterService;

        }

        /// <summary>
        ///     Get all Franchises in the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchisesServise.GetAllFranchiseAsync());
        }

        /// <summary>
        /// Gets a specific Franchise by their ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            if (!_franchisesServise.FranchiseExists(id))
            {
                return NotFound($"Franchise with id={id} not found");
            }

            Franchise franchise = await _franchisesServise.GetSpecificFranchiseAsync(id);

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        ///     Updates a Franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchiseDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDTO)
        {
            if (id != franchiseDTO.ID)
            {
                return BadRequest($"Could no update!! Franchise with id = {id} does not exist");
            }

            if(!_franchisesServise.FranchiseExists(id))
            {
                return NotFound($"Franchise with id = {id} not found");
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDTO);
            await _franchisesServise.UpdateFranchiseAsync(domainFranchise);
           

            return NoContent();
        }

        /// <summary>
        ///     Adds a new Franchise to the database.
        /// </summary>
        /// <param name="franchiseDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchiseDTO)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDTO);
            domainFranchise = await _franchisesServise.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise", 
                new { id = domainFranchise.ID }, 
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        ///     Deletes a Franchise from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchisesServise.FranchiseExists(id))
            {
                return NotFound($"Franchise with id = {id} not found");
            }

            await _franchisesServise.DeleteFranchiseAsync(id);
            

            return NoContent();

        }

        /// <summary>
        /// Get all Movies in Franchise.
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns>All movies in this franchise.</returns>
        [HttpGet("movies/{id}")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMoviesInFranchise(int id)
        {
            FranchiseReadDTO franchise = _mapper.Map<FranchiseReadDTO>(await _franchisesServise.GetSpecificFranchiseAsync(id));
            List<MovieReadDTO> franchiseMovies = new();


            if (!_franchisesServise.FranchiseExists(id))
            {
                return NotFound($"Franchise with id={id} not found");
            }

            foreach (int movieID in franchise.Movies)
            {
                MovieReadDTO movie = _mapper.Map<MovieReadDTO>(await _movieService.GetSpecificMovieAsync(movieID));
                
                if (!_movieService.MovieExists(movieID))
                {
                    return NotFound($"Movie with id={movieID} that is listed in this franchise's movies is not found");
                }

                franchiseMovies.Add(movie);
            }

            return franchiseMovies;
        }

        /// <summary>
        /// Get all Characters in Franchise
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns>All characters in this franchise</returns>
        [HttpGet("characters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInFranchise(int id)
        {
            List<CharacterReadDTO> franchiseCharacters = new();

            if (!_franchisesServise.FranchiseExists(id))
            {
                return NotFound($"Franchise with id={id} not found");
            }

            FranchiseReadDTO franchise = _mapper.Map<FranchiseReadDTO>(await _franchisesServise.GetSpecificFranchiseAsync(id));

            HashSet<int> franchiseCharacterIDs = new ();

            foreach (int movieID in franchise.Movies)
            {
                MovieReadDTO movie = _mapper.Map<MovieReadDTO>(await _movieService.GetSpecificMovieAsync(movieID));
                if (movie == null)
                {
                    return NotFound($"Movie with id={movieID} that is listed in this franchise's movies is not found");
                }
                franchiseCharacterIDs.UnionWith(movie.Characters);
                
            }
 
            foreach (int characterID in franchiseCharacterIDs)
            {

                CharacterReadDTO character = _mapper.Map<CharacterReadDTO>(await _characterService.GetSpecificCharacterAsync(characterID));
               
                if (character == null)
                {
                   return NotFound($"Character with id={characterID} is not found");
                }

                    franchiseCharacters.Add(character);
            }

            return franchiseCharacters;
        }



        /// <summary>
        /// Updates Movies in Franchise.
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <param name="movieIDs">List of Movie IDs of the movies you want this franchise to have</param>
        /// <returns></returns>
        [HttpPut("movie/{id}")]
        public async Task<IActionResult> PutMoviesInFranchise(int id, List<int> movieIDs)
        {
            if (!_franchisesServise.FranchiseExists(id))
            {
                return NotFound($"Franchise with id={id} not found");
            }

            try
            {
                await _franchisesServise.UpdateFranchiseMoviesAsync(id, movieIDs);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

    }
}
