﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.MovieDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            // Movie <-> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                // Turning related athletes into int arrays
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.ID).ToList()))
                .ReverseMap();

            // MovieCreateDTO <-> Movie
            CreateMap<Movie, MovieCreateDTO>().ReverseMap();

            // MovieEditDTO <-> Movie
            CreateMap<Movie, MovieEditDTO>().ReverseMap();

        }
    }
}
