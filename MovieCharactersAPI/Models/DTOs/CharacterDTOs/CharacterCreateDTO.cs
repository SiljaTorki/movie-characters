﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTOs.CharacterDTOs
{
    public class CharacterCreateDTO
    {
        // same as characterReadDTO but without ID
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
