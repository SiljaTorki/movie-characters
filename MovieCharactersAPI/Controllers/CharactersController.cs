﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Data;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.CharacterDTOs;
using MovieCharactersAPI.Services;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharactersDbContext _context;
        // Add automapper thorugh DI
        private readonly IMapper _mapper;
        private readonly ICharacterService _service;

        public CharactersController(MovieCharactersDbContext context, IMapper mapper, ICharacterService characterService)
        {
            _context = context;
            _mapper = mapper;
            _service = characterService;
        }

        /// <summary>
        /// Get all Characters in the database.
        /// </summary>
        /// <returns>List of all Characters in the database.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacter()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _service.GetAllCharacterAsync());
        }

        /// <summary>
        /// Gets a specific Character by their ID.
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns>Character</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            if (!_service.CharacterExists(id)) return NotFound($"Character with id={id} not found.");

            return _mapper.Map<CharacterReadDTO>(await _service.GetSpecificCharacterAsync(id));
        }

        /// <summary>
        /// Updates a Character.
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <param name="characterDTO">CharacterEditDTO with updated attributes.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDTO)
        { 
            if (id != characterDTO.ID)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(characterDTO);

            try
            {
                await _service.UpdateCharacterAsync(domainCharacter);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!_service.CharacterExists(id))
                {
                    return NotFound($"Character with id={id} not found.");
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new Character to the database.
        /// </summary>
        /// <param name="characterDTO">CharacterCreateDTO</param>
        /// <returns>The character object if added successfully to database.</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO characterDTO)
        {
            Character domainCharacter = _mapper.Map<Character>(characterDTO);

            try
            {
                await _service.AddCharacterAsync(domainCharacter);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return CreatedAtAction("GetCharacter", 
                new { id = domainCharacter.ID},
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        ///     Deletes a Character from the database.
        /// </summary>
        /// <param name="id">Character ID</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_service.CharacterExists(id)) return NotFound($"Character with id={id} not found.");

            try
            {
                await _service.DeleteCharacterAsync(id);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }
    }
}
