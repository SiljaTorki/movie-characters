﻿using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.MovieDTOs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    public interface IMovieService
    {
        /// <summary>
        /// Get all movies.
        /// </summary>
        /// <returns>Collection of all movies in database.</returns>
        public Task<IEnumerable<Movie>> GetAllMovieAsync();

        /// <summary>
        /// Get a movie by id.
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns>Movie with the given id, if it exists in the database.</returns>
        public Task<Movie> GetSpecificMovieAsync(int id);

        /// <summary>
        /// Add movie to the database.
        /// </summary>
        /// <param name="movie">Movie to add</param>
        /// <returns>The movie added into the database if successful.</returns>
        public Task<Movie> AddMovieAsync(Movie movie);

        /// <summary>
        /// Update movie in database.
        /// </summary>
        /// <param name="movie">Movie with updated attributes.</param>
        /// <returns></returns>
        public Task UpdateMovieAsync(Movie movie);

        /// <summary>
        /// Delete a movie from the database.
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns></returns>
        public Task DeleteMovieAsync(int id);

        /// <summary>
        /// Check if movie with the given ID exists in the database.
        /// </summary>
        /// <param name="id">Movie ID</param>
        /// <returns>True if movie exists.</returns>
        public bool MovieExists(int id);

    }
}
