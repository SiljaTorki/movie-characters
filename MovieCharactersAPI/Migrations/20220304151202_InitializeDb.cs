﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharactersAPI.Migrations
{
    public partial class InitializeDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    FranchiseID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Movie_Franchise_FranchiseID",
                        column: x => x.FranchiseID,
                        principalTable: "Franchise",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "CharacterMovie",
                columns: table => new
                {
                    MoviesID = table.Column<int>(type: "int", nullable: false),
                    CharactersID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CharacterMovie", x => new { x.MoviesID, x.CharactersID });
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Character_CharactersID",
                        column: x => x.CharactersID,
                        principalTable: "Character",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CharacterMovie_Movie_MoviesID",
                        column: x => x.MoviesID,
                        principalTable: "Movie",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "ID", "Alias", "Gender", "Name", "Picture" },
                values: new object[,]
                {
                    { 1, null, "female", "Rose DeWitt Bukater", "https://titanic.fandom.com/wiki/Rose_DeWitt_Bukater?file=Rose-dewittbukater-1997film.jpg" },
                    { 2, null, "male", "Jack Dawson", "https://titanic.fandom.com/wiki/Jack_Dawson?file=Jack-Dawson-1997film.jpg" },
                    { 3, "Gunnar", "male", "T-Rex", "https://jurassicpark.fandom.com/wiki/Jurassic_Park_(film)?file=DVDPlay_2009-05-23_16-48-45-52.jpg" },
                    { 4, null, "male", "Dr. Alan Grant", "https://jurassicpark.fandom.com/wiki/Alan_Grant?file=GrantandThePhone.png" },
                    { 5, null, "female", "Dr. Ellie Sattler", "https://jurassicpark.fandom.com/wiki/Ellie_Sattler?file=Ellie-run.jpg" },
                    { 6, null, "male", "Dr. Ian Malcolm", "https://jurassicpark.fandom.com/wiki/Ian_Malcolm?file=JP-IanMalcolm.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "A boat sinks, how many movies can we make of this?", "Titanic" },
                    { 2, "Some rich fool made a dinosaur amusement park, watch chaos unfold.", "Jurassic" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "ID", "Director", "FranchiseID", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "James Cameron", 1, "drama, romance, tragedy", "https://titanic.fandom.com/wiki/Titanic_(1997_film)?file=Titanic_ver2_xlg.jpg", 1997, "Titanic", "https://www.youtube.com/watch?v=kEPfM3jSoBw" },
                    { 2, "Steven Spielberg", 2, "drama, tragedy, violence, dinosaurs", "https://jurassicpark.fandom.com/wiki/Jurassic_Park_(film)?file=JP-MoviePoster.jpg", 1993, "Jurassic Park", "https://www.youtube.com/watch?v=W85oD8FEF78" },
                    { 3, "Steven Spielberg", 2, "drama, tragedy, violence, dinosaurs", "https://jurassicpark.fandom.com/wiki/The_Lost_World:_Jurassic_Park_(film)?file=TLW-MoviePoster.jpg", 1997, "The Lost World: Jurassic Park", "https://www.youtube.com/watch?v=nq8FYD0FACk" },
                    { 4, "Steven Spielberg", 2, "drama, tragedy, violence, dinosaurs", "https://jurassicpark.fandom.com/wiki/Jurassic_Park_III?file=Jurassic_Park_III_Poster.jpg", 2001, "Jurassic Park III", "https://www.youtube.com/watch?v=zflHpbUyr3M" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersID", "MoviesID" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 2 },
                    { 4, 2 },
                    { 5, 2 },
                    { 6, 2 },
                    { 3, 3 },
                    { 6, 3 },
                    { 3, 4 },
                    { 4, 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_CharactersID",
                table: "CharacterMovie",
                column: "CharactersID");

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseID",
                table: "Movie",
                column: "FranchiseID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CharacterMovie");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
