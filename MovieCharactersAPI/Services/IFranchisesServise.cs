﻿using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    public interface IFranchisesServise
    {
        /// <summary>
        /// Get all Franchises.
        /// </summary>
        /// <returns>Collection of all Franchises in the database.</returns>
        public Task<IEnumerable<Franchise>> GetAllFranchiseAsync();

        /// <summary>
        /// Get Franchise by ID.
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        public Task<Franchise> GetSpecificFranchiseAsync(int id);

        /// <summary>
        /// Add Franchise to database.
        /// </summary>
        /// <param name="franchise">Character to add</param>
        /// <returns>The Franchise added into the database if successful.</returns>
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Update Franchise in database.
        /// </summary>
        /// <param name="franchise">Franchiser with updated attributes</param>
        /// <returns></returns>
        public Task UpdateFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Delete a Franchise from the database.
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns></returns>
        public Task DeleteFranchiseAsync(int id);

        /// <summary>
        /// Update which movies belong to this franchise.
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <param name="movieIDs">List of movie IDs</param>
        /// <returns></returns>
        public Task UpdateFranchiseMoviesAsync(int id, List<int> movieIDs);

        /// <summary>
        /// Check if Franchise with the given ID exists in the database.
        /// </summary>
        /// <param name="id">Franchise ID</param>
        /// <returns>True if Franchise exists.</returns>
        public bool FranchiseExists(int id);

    }
}
