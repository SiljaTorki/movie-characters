﻿using MovieCharactersAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTOs.FranchiseDTOs
{
    public class FranchiseReadDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<int> Movies { get; set; }
    }
}
