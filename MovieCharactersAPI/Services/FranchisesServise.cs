﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Data;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.CharacterDTOs;
using MovieCharactersAPI.Models.DTOs.FranchiseDTOs;
using MovieCharactersAPI.Models.DTOs.MovieDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Services
{
    public class FranchisesServise : IFranchisesServise
    {
        private readonly MovieCharactersDbContext _context;
        private readonly IMovieService _movieService;

        public FranchisesServise(MovieCharactersDbContext context, IMovieService movieService)
        {
            _context = context;
            _movieService = movieService;
        }


        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);
            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.ID == id);
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchiseAsync()
        {
            return await _context.Franchise.Include(f => f.Movies).ToListAsync();
        }

        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchise.Include(f => f.Movies).FirstOrDefaultAsync(f => f.ID == id);
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateFranchiseMoviesAsync(int id, List<int> movieIDs)
        {
            Franchise franchiseToUpdate = await _context.Franchise
                .Include(f => f.Movies)
                .Where(f => f.ID == id)
                .FirstAsync();

            List<Movie> movies = new();
            foreach (int movieID in movieIDs)
            {
                Movie movie = await _context.Movie.FindAsync(movieID);
                if (movie == null)
                {
                    throw new Exception($"Movie with id={movieID} is not found");
                }
                movies.Add(movie);
            }

            franchiseToUpdate.Movies = movies;
            await _context.SaveChangesAsync();
        }
    }
}
