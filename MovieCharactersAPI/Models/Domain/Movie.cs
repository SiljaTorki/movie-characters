﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(200)]
        public string Title { get; set; }
        [MaxLength(200)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(200)]
        public string Picture { get; set; }
        [MaxLength(200)]
        public string Trailer { get; set; }

        //Relationships
        public ICollection<Character> Characters { get; set; } = new HashSet<Character>();

        [ForeignKey("FranchiseID")]
        public int? FranchiseID { get; set; }
        public Franchise Franchise { get; set; }

    }
}

