﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.FranchiseDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            // Franchise <-> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                // Turning related movies into arrays
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.ID).ToArray()))
                .ReverseMap();

            // FranchiseCreateDTO <-> Franchise
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();

            // FranchiseEditDTO <-> Franchise
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();
        }
    }
}
