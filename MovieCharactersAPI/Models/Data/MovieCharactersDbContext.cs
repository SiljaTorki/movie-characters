﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Domain;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MovieCharactersAPI.Models.Data
{
    public class MovieCharactersDbContext : DbContext
    {
        public DbSet<Character> Character { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Franchise> Franchise { get; set; }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //Data Source -> Cnnect to Local Server (must change to correct local server)
            //Initial Cataog -> Name on databasce created
            optionsBuilder.UseSqlServer("Data Source = ND-5CG0257SQQ\\SQLEXPRESS; Initial Catalog =  MovieCharactersDb; Integrated Security = true");  
        }*/

        public MovieCharactersDbContext([NotNullAttribute] DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // One 2 Many delete. 
            modelBuilder.Entity<Franchise>()
                .HasMany(f => f.Movies)
                .WithOne(m => m.Franchise)
                .OnDelete(DeleteBehavior.SetNull);


            // Seeding the DB with data
            // Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise { ID = 1, Name = "Titanic", Description = "A boat sinks, how many movies can we make of this?" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { ID = 2, Name = "Jurassic", Description = "Some rich fool made a dinosaur amusement park, watch chaos unfold." });

            // Movies
            modelBuilder.Entity<Movie>().HasData(new Domain.Movie
            {
                ID = 1,
                Title = "Titanic",
                Director = "James Cameron",
                Genre = "drama, romance, tragedy",
                FranchiseID = 1,
                Picture = "https://titanic.fandom.com/wiki/Titanic_(1997_film)?file=Titanic_ver2_xlg.jpg",
                ReleaseYear = 1997,
                Trailer = "https://www.youtube.com/watch?v=kEPfM3jSoBw"
            });
            modelBuilder.Entity<Movie>().HasData(new Domain.Movie
            {
                ID = 2,
                Title = "Jurassic Park",
                Director = "Steven Spielberg",
                Genre = "drama, tragedy, violence, dinosaurs",
                FranchiseID = 2,
                Picture = "https://jurassicpark.fandom.com/wiki/Jurassic_Park_(film)?file=JP-MoviePoster.jpg",
                ReleaseYear = 1993,
                Trailer = "https://www.youtube.com/watch?v=W85oD8FEF78"
            });
            modelBuilder.Entity<Movie>().HasData(new Domain.Movie
            {
                ID = 3,
                Title = "The Lost World: Jurassic Park",
                Director = "Steven Spielberg",
                Genre = "drama, tragedy, violence, dinosaurs",
                FranchiseID = 2,
                Picture = "https://jurassicpark.fandom.com/wiki/The_Lost_World:_Jurassic_Park_(film)?file=TLW-MoviePoster.jpg",
                ReleaseYear = 1997,
                Trailer = "https://www.youtube.com/watch?v=nq8FYD0FACk"
            });
            modelBuilder.Entity<Movie>().HasData(new Domain.Movie
            {
                ID = 4,
                Title = "Jurassic Park III",
                Director = "Steven Spielberg",
                Genre = "drama, tragedy, violence, dinosaurs",
                FranchiseID = 2,
                Picture = "https://jurassicpark.fandom.com/wiki/Jurassic_Park_III?file=Jurassic_Park_III_Poster.jpg",
                ReleaseYear = 2001,
                Trailer = "https://www.youtube.com/watch?v=zflHpbUyr3M"
            });

            // Characters
            modelBuilder.Entity<Character>().HasData(new Domain.Character { ID = 1, Name = "Rose DeWitt Bukater", Gender = "female", Picture = "https://titanic.fandom.com/wiki/Rose_DeWitt_Bukater?file=Rose-dewittbukater-1997film.jpg" });
            modelBuilder.Entity<Character>().HasData(new Domain.Character { ID = 2, Name = "Jack Dawson", Gender = "male", Picture = "https://titanic.fandom.com/wiki/Jack_Dawson?file=Jack-Dawson-1997film.jpg" });
            modelBuilder.Entity<Character>().HasData(new Domain.Character { ID = 3, Name = "T-Rex", Gender = "male", Alias = "Gunnar", Picture = "https://jurassicpark.fandom.com/wiki/Jurassic_Park_(film)?file=DVDPlay_2009-05-23_16-48-45-52.jpg" });
            modelBuilder.Entity<Character>().HasData(new Domain.Character { ID = 4, Name = "Dr. Alan Grant", Gender = "male", Picture = "https://jurassicpark.fandom.com/wiki/Alan_Grant?file=GrantandThePhone.png" });
            modelBuilder.Entity<Character>().HasData(new Domain.Character { ID = 5, Name = "Dr. Ellie Sattler", Gender = "female", Picture = "https://jurassicpark.fandom.com/wiki/Ellie_Sattler?file=Ellie-run.jpg" });
            modelBuilder.Entity<Character>().HasData(new Domain.Character { ID = 6, Name = "Dr. Ian Malcolm", Gender = "male", Picture = "https://jurassicpark.fandom.com/wiki/Ian_Malcolm?file=JP-IanMalcolm.jpg" });


            // Setting relationships between movies and characters
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
               "CharacterMovie",
               r => r.HasOne<Character>().WithMany().HasForeignKey("CharactersID"),
               l => l.HasOne<Movie>().WithMany().HasForeignKey("MoviesID"),
               je =>
               {
                   je.HasKey("MoviesID", "CharactersID");
                   je.HasData(
                       new { MoviesID = 1, CharactersID = 1 },
                       new { MoviesID = 1, CharactersID = 2 },
                       new { MoviesID = 2, CharactersID = 3 },
                       new { MoviesID = 2, CharactersID = 4 },
                       new { MoviesID = 2, CharactersID = 5 },
                       new { MoviesID = 2, CharactersID = 6 },
                       new { MoviesID = 3, CharactersID = 3 },
                       new { MoviesID = 3, CharactersID = 6 },
                       new { MoviesID = 4, CharactersID = 3 },
                       new { MoviesID = 4, CharactersID = 4 }
                       );
               });
        }
    }
}
